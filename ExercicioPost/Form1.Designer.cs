﻿namespace ExercicioPost
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelTitulo = new System.Windows.Forms.Label();
            this.TextBoxTitulo = new System.Windows.Forms.TextBox();
            this.LabelDescricao = new System.Windows.Forms.Label();
            this.TextBoxDescricao = new System.Windows.Forms.TextBox();
            this.ButtonPost = new System.Windows.Forms.Button();
            this.ButtonLike = new System.Windows.Forms.Button();
            this.ButtonDontLike = new System.Windows.Forms.Button();
            this.LabelData = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelLikeCount = new System.Windows.Forms.Label();
            this.LabelDontLikeCount = new System.Windows.Forms.Label();
            this.ButtonNovoPost = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LabelTitulo
            // 
            this.LabelTitulo.AutoSize = true;
            this.LabelTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTitulo.Location = new System.Drawing.Point(57, 27);
            this.LabelTitulo.Name = "LabelTitulo";
            this.LabelTitulo.Size = new System.Drawing.Size(51, 20);
            this.LabelTitulo.TabIndex = 0;
            this.LabelTitulo.Text = "Título:";
            // 
            // TextBoxTitulo
            // 
            this.TextBoxTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxTitulo.Location = new System.Drawing.Point(120, 25);
            this.TextBoxTitulo.Name = "TextBoxTitulo";
            this.TextBoxTitulo.Size = new System.Drawing.Size(310, 24);
            this.TextBoxTitulo.TabIndex = 1;
            // 
            // LabelDescricao
            // 
            this.LabelDescricao.AutoSize = true;
            this.LabelDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDescricao.Location = new System.Drawing.Point(24, 69);
            this.LabelDescricao.Name = "LabelDescricao";
            this.LabelDescricao.Size = new System.Drawing.Size(84, 20);
            this.LabelDescricao.TabIndex = 2;
            this.LabelDescricao.Text = "Descrição:";
            // 
            // TextBoxDescricao
            // 
            this.TextBoxDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxDescricao.Location = new System.Drawing.Point(120, 71);
            this.TextBoxDescricao.Multiline = true;
            this.TextBoxDescricao.Name = "TextBoxDescricao";
            this.TextBoxDescricao.Size = new System.Drawing.Size(310, 113);
            this.TextBoxDescricao.TabIndex = 3;
            // 
            // ButtonPost
            // 
            this.ButtonPost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonPost.Location = new System.Drawing.Point(355, 237);
            this.ButtonPost.Name = "ButtonPost";
            this.ButtonPost.Size = new System.Drawing.Size(75, 29);
            this.ButtonPost.TabIndex = 4;
            this.ButtonPost.Text = "Post";
            this.ButtonPost.UseVisualStyleBackColor = true;
            this.ButtonPost.Click += new System.EventHandler(this.ButtonPost_Click);
            // 
            // ButtonLike
            // 
            this.ButtonLike.Enabled = false;
            this.ButtonLike.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonLike.Location = new System.Drawing.Point(120, 236);
            this.ButtonLike.Name = "ButtonLike";
            this.ButtonLike.Size = new System.Drawing.Size(75, 30);
            this.ButtonLike.TabIndex = 5;
            this.ButtonLike.Text = "Gosto";
            this.ButtonLike.UseVisualStyleBackColor = true;
            this.ButtonLike.Click += new System.EventHandler(this.ButtonLike_Click);
            // 
            // ButtonDontLike
            // 
            this.ButtonDontLike.Enabled = false;
            this.ButtonDontLike.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonDontLike.Location = new System.Drawing.Point(216, 236);
            this.ButtonDontLike.Name = "ButtonDontLike";
            this.ButtonDontLike.Size = new System.Drawing.Size(97, 30);
            this.ButtonDontLike.TabIndex = 6;
            this.ButtonDontLike.Text = "Não Gosto";
            this.ButtonDontLike.UseVisualStyleBackColor = true;
            this.ButtonDontLike.Click += new System.EventHandler(this.ButtonDontLike_Click);
            // 
            // LabelData
            // 
            this.LabelData.AutoSize = true;
            this.LabelData.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelData.Location = new System.Drawing.Point(352, 198);
            this.LabelData.Name = "LabelData";
            this.LabelData.Size = new System.Drawing.Size(48, 18);
            this.LabelData.TabIndex = 8;
            this.LabelData.Text = "../../....";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(118, 200);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = "Like:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(204, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "Don\'t like";
            // 
            // LabelLikeCount
            // 
            this.LabelLikeCount.AutoSize = true;
            this.LabelLikeCount.Location = new System.Drawing.Point(157, 202);
            this.LabelLikeCount.Name = "LabelLikeCount";
            this.LabelLikeCount.Size = new System.Drawing.Size(13, 13);
            this.LabelLikeCount.TabIndex = 11;
            this.LabelLikeCount.Text = "0";
            // 
            // LabelDontLikeCount
            // 
            this.LabelDontLikeCount.AutoSize = true;
            this.LabelDontLikeCount.Location = new System.Drawing.Point(268, 202);
            this.LabelDontLikeCount.Name = "LabelDontLikeCount";
            this.LabelDontLikeCount.Size = new System.Drawing.Size(13, 13);
            this.LabelDontLikeCount.TabIndex = 12;
            this.LabelDontLikeCount.Text = "0";
            // 
            // ButtonNovoPost
            // 
            this.ButtonNovoPost.Enabled = false;
            this.ButtonNovoPost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonNovoPost.Location = new System.Drawing.Point(12, 230);
            this.ButtonNovoPost.Name = "ButtonNovoPost";
            this.ButtonNovoPost.Size = new System.Drawing.Size(75, 56);
            this.ButtonNovoPost.TabIndex = 13;
            this.ButtonNovoPost.Text = "Novo Post";
            this.ButtonNovoPost.UseVisualStyleBackColor = true;
            this.ButtonNovoPost.Click += new System.EventHandler(this.ButtonNovoPost_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 298);
            this.Controls.Add(this.ButtonNovoPost);
            this.Controls.Add(this.LabelDontLikeCount);
            this.Controls.Add(this.LabelLikeCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LabelData);
            this.Controls.Add(this.ButtonDontLike);
            this.Controls.Add(this.ButtonLike);
            this.Controls.Add(this.ButtonPost);
            this.Controls.Add(this.TextBoxDescricao);
            this.Controls.Add(this.LabelDescricao);
            this.Controls.Add(this.TextBoxTitulo);
            this.Controls.Add(this.LabelTitulo);
            this.Name = "Form1";
            this.Text = "Post";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelTitulo;
        private System.Windows.Forms.TextBox TextBoxTitulo;
        private System.Windows.Forms.Label LabelDescricao;
        private System.Windows.Forms.TextBox TextBoxDescricao;
        private System.Windows.Forms.Button ButtonPost;
        private System.Windows.Forms.Button ButtonLike;
        private System.Windows.Forms.Button ButtonDontLike;
        private System.Windows.Forms.Label LabelData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelLikeCount;
        private System.Windows.Forms.Label LabelDontLikeCount;
        private System.Windows.Forms.Button ButtonNovoPost;
    }
}

