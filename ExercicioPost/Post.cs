﻿namespace ExercicioPost
{
    using System;
    class Post
    {
        #region Atributos

        private string _titulo;
        private string _descricao;
        private DateTime _data;
        private int _likes;
        private int _dontLikes;

        #endregion

        #region Propriedades

        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public DateTime Data { get; private set; }
        public int Likes { get; private set; }
        public int DontLikes { get; private set; }

        #endregion

        #region Métodos

        public Post(string titulo, string descricao)
        {
            Titulo = titulo;
            Descricao = descricao;
        }

        public void SomaLikes()
        {
            Likes++;
        }
        public void SomaDontLikes()
        {
            DontLikes++;
        }
        public void AtribuiData()
        {
            Data = DateTime.Today;
          
        }
        #endregion
    }
}
