﻿namespace ExercicioPost
{
    using System;
    using System.Windows.Forms;
    public partial class Form1 : Form
    {
        Post post;
        public Form1()
        {
            InitializeComponent();
                  
        }

        private void ButtonPost_Click(object sender, EventArgs e)
        {
            if(TextBoxTitulo.Text.Trim() == "")
            {
                MessageBox.Show("Insira um título!");
                return;
            }
            else if(TextBoxDescricao.Text.Trim() == "")
            {
                MessageBox.Show("Insira uma descrição!");
                return;
            }

            post = new Post(TextBoxTitulo.Text.Trim(), TextBoxDescricao.Text.Trim());
            post.AtribuiData();
            LabelData.Text = String.Format("{0}/{1}/{2}", post.Data.Day, post.Data.Month, post.Data.Year);
            ButtonPost.Enabled = false;
            ButtonLike.Enabled = true;
            ButtonDontLike.Enabled = true;
            TextBoxTitulo.Enabled = false;
            TextBoxDescricao.Enabled = false;
            ButtonNovoPost.Enabled = true;
        }

        private void ButtonLike_Click(object sender, EventArgs e)
        {
            post.SomaLikes();
            LabelLikeCount.Text = post.Likes.ToString();
        }

        private void ButtonDontLike_Click(object sender, EventArgs e)
        {
            post.SomaDontLikes();
            LabelDontLikeCount.Text = post.DontLikes.ToString();
        }

        private void ButtonNovoPost_Click(object sender, EventArgs e)
        {
            post = null;
            TextBoxTitulo.Enabled = true;
            TextBoxDescricao.Enabled = true;
            TextBoxTitulo.Text = "";
            TextBoxDescricao.Text = "";
            ButtonPost.Enabled = true;
            LabelLikeCount.Text = "0";
            LabelDontLikeCount.Text = "0";
            LabelData.Text = "../../....";
            ButtonLike.Enabled = false;
            ButtonDontLike.Enabled = false;
            ButtonNovoPost.Enabled = false;
        }
    }
}
